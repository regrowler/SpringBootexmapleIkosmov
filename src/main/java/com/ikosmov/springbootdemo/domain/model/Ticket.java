package com.ikosmov.springbootdemo.domain.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.IOException;
import java.util.Objects;

@Entity
public class Ticket {
    public Ticket(Long id, Long ticketPassengerId, Long ticketFlightId) {
        this.id = id;
        this.ticketPassengerId = ticketPassengerId;
        this.ticketFlightId = ticketFlightId;
    }
    public Ticket(int id, int ticketPassengerId, int ticketFlightId) {
        this.id = new Long(id);
        this.ticketPassengerId = new Long(ticketPassengerId);
        this.ticketFlightId = new Long(ticketFlightId);
    }

    @Id
    private Long id;
    public Long ticketPassengerId;
    private Long ticketFlightId;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Passenger passenger;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Flight flight;

    public Ticket() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long ticket_id) {
        this.id = ticket_id;
    }

    public Long getTicketFlightId() {
        return ticketFlightId;
    }

    public void setTicketFlightId(Long flight_id) {
        this.ticketFlightId = flight_id;
    }

    public Long getTicketPassengerId() {
        return ticketPassengerId;
    }

    public void setTicketPassengerId(Long passenger_id) {
        this.ticketPassengerId = passenger_id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Ticket{" +
                "id=" + id +
                ", ticketPassengerId=" + ticketPassengerId +
                ", ticketFlightId=" + ticketFlightId +
                ", passenger=" + passenger +
                ", flight=" + flight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id.equals(ticket.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
