package com.ikosmov.springbootdemo.domain.dao;


import com.ikosmov.springbootdemo.domain.model.Flight;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.domain.repository.PassengerRepository;
import com.ikosmov.springbootdemo.utils.Response;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PassengerDao {

    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    FlightDao flightDao;
    @Autowired
    TicketDao ticketDao;

    public List<Passenger> getPassengers() {
        return passengerRepository.findAll();
    }

    public Passenger getPassenger(Long id) {
        return passengerRepository.findById(new Long(id)).orElse(new Passenger());
    }

    public Response postPassenger(Long id, String name) {
        Passenger passenger = new Passenger(id, name);
        passengerRepository.save(passenger);
        return new Response("ok", passenger);
    }

    public Response postPassenger(String name) {
        List<Passenger> list = getPassengers();
        Passenger passenger = new Passenger(list.get(list.size() - 1).getId() + 1, name);
        passengerRepository.save(passenger);
        return new Response("ok", passenger);
    }

    public Response deletePassenger(Long id) {
        Passenger passenger = getPassenger(id);
        passengerRepository.deleteById(new Long(id));
        return new Response("ok", passenger);

    }

    public Response addFlight(Long passengerId, Long id) {
        Optional<Passenger> passenger = passengerRepository.findById(new Long(passengerId));
        if (!passenger.isPresent()) return null;
        Flight flight = flightDao.getFlight(id);
        if (flight != null) {
            passenger.get().getFlights().add(flight);
            return new Response("ok", flight);
        }
        return new Response("ticket not found", null);

    }

    public Response removeFlight(Passenger passenger1, Long id) {
        Optional<Passenger> passenger = Optional.of(passenger1);
        if (!passenger.isPresent()) return new Response("ticket not found", null);
        Flight flight = flightDao.getFlight(id);
        if (flight != null && passenger.get().getFlights().contains(flight)) {
            passenger.get().getFlights().add(flight);
            return new Response("ok", flight);
        }
        return new Response("ticket not found", null);
    }

    public Response removeTicket(Passenger passenger1, Long id) {
        Optional<Passenger> passenger = Optional.of(passenger1);
        if (!passenger.isPresent()) return new Response("ticket not found", null);
        Ticket ticket = ticketDao.getTicket(id);
        if (ticket != null && passenger.get().getTickets().contains(ticket)) {
            passenger.get().getTickets().add(ticket);
            return new Response("ok", ticket);
        }
        return new Response("ticket not found", null);
    }

    public Response addTicket(Long passengerId, Long id) {
        Optional<Passenger> passenger = passengerRepository.findById(new Long(passengerId));
        if (!passenger.isPresent()) return null;
        Ticket ticket = ticketDao.getTicket(id);
        if (ticket != null) {
            passenger.get().getTickets().add(ticket);
            return new Response("ok", ticket);
        }
        return new Response("ticket not found", null);
    }

    public List<Ticket> getPassengersTickets(Long id) {
        Optional<Passenger> passenger = passengerRepository.findById(new Long(id));
        if (!passenger.isPresent()) return null;
        List list=passenger.get().getTickets();
        return list;
    }

    public Set<Flight> getPassengersFlights(Long id) {
        Optional<Passenger> passenger = passengerRepository.findById(new Long(id));
        if (!passenger.isPresent()) return null;
        Set set=passenger.get().getFlights();
        return set;
    }

}
