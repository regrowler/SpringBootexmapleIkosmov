package com.ikosmov.springbootdemo.controller;


import com.ikosmov.springbootdemo.domain.dao.FlightDao;
import com.ikosmov.springbootdemo.domain.dao.PassengerDao;
import com.ikosmov.springbootdemo.domain.dao.TicketDao;
import com.ikosmov.springbootdemo.domain.model.Flight;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.domain.repository.FlightRepository;
import com.ikosmov.springbootdemo.domain.repository.PassengerRepository;
import com.ikosmov.springbootdemo.domain.repository.TicketRepository;
import com.ikosmov.springbootdemo.utils.DataShow;
import com.ikosmov.springbootdemo.utils.ListGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;

@Controller
public class MainController {
    @Autowired
    DataShow dataShow;
    //private static final Log LOG = LogFactory.getLog(MainController.class);
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    FlightRepository flightRepository;
    @Autowired
    FlightDao flightDao;
    @Autowired
    PassengerDao passengerDao;
    @Autowired
    TicketDao ticketDao;

    @RequestMapping("/")
    public void index(HttpServletResponse response) throws IOException {
        response.sendRedirect("/index.html");
    }

    @RequestMapping("/init")
    public void init(HttpServletResponse response) throws IOException {
        Passenger passenger1 = new Passenger(new Long(1), "John");
        Passenger passenger2 = new Passenger(new Long(2), "Andrew");
        Passenger passenger3 = new Passenger(new Long(3), "Michael");
        Passenger passenger5 = new Passenger(new Long(4), "DIma");
        Stream.of(passenger1, passenger2, passenger3, passenger5).forEach(passengerRepository::save);
        dataShow.showAllData();
        Ticket ticket1 = new Ticket(new Long(1), new Long(1), new Long(1));
        Ticket ticket2 = new Ticket(new Long(2), new Long(2), new Long(1));
        Ticket ticket3 = new Ticket(new Long(3), new Long(3), new Long(1));
        Ticket ticket4 = new Ticket(new Long(4), new Long(4), new Long(2));
        Stream.of(ticket1, ticket2, ticket3, ticket4).forEach(ticketRepository::save);
        dataShow.showAllData();
        Flight flight1 = new Flight(new Long(1), "Samara", "Moscow");
        Flight flight2 = new Flight(new Long(2), "Samara", "Saratov");
        Stream.of(flight1, flight2).forEach(flightRepository::save);
        dataShow.showAllData();
//        passenger1 = passengerDao.getPassenger(new Long(1));
//        passenger2 = passengerDao.getPassenger(new Long(2));
//        passenger3 = passengerDao.getPassenger(new Long(3));
//        passenger5 = passengerDao.getPassenger(new Long(4));
//        ticket1 = ticketDao.getTicket(new Long(1));
//        ticket2 = ticketDao.getTicket(new Long(2));
//        ticket3 = ticketDao.getTicket(new Long(3));
//        ticket4 = ticketDao.getTicket(new Long(4));
//        flight1 = flightDao.getFlight(new Long(1));
//        flight2 = flightDao.getFlight(new Long(2));

        passenger1.setTickets(ListGetter.getListFromEntity(ticket1));
        passenger2.setTickets(ListGetter.getListFromEntity(ticket2));
        passenger3.setTickets(ListGetter.getListFromEntity(ticket3));
        passenger5.setTickets(ListGetter.getListFromEntity(ticket4));
        passenger1.setFlights(ListGetter.getSetFromEntity(flight1, flight2));
        passenger2.setFlights(ListGetter.getSetFromEntity(flight1));
        passenger3.setFlights(ListGetter.getSetFromEntity(flight1));
        passenger5.setFlights(ListGetter.getSetFromEntity(flight2));
        Stream.of(passenger1, passenger2, passenger3, passenger5).forEach(passengerRepository::save);
        flight1.setPassengers(ListGetter.getSetFromEntity(passenger1, passenger2, passenger3));
        flight2.setPassengers(ListGetter.getSetFromEntity(passenger5));
        Stream.of(flight1, flight2).forEach(flightRepository::save);

        passenger1 = new Passenger(new Long(1), "John");
        passenger2 = new Passenger(new Long(2), "Andrew");
        passenger3 = new Passenger(new Long(3), "Michael");
        passenger5 = new Passenger(new Long(4), "DIma");
        dataShow.showAllData();
        ticket1.setPassenger(passenger1);
        ticket1.setFlight(flight1);
        ticket2.setPassenger(passenger2);
        ticket2.setFlight(flight1);
        ticket3.setPassenger(passenger3);
        ticket3.setFlight(flight1);
        ticket4.setPassenger(passenger5);
        ticket4.setFlight(flight2);
        Stream.of(ticket1, ticket2, ticket3, ticket4).forEach(t -> {
            ticketRepository.save(t);
            dataShow.showFlightsTickets();
        });
        ticket1 = new Ticket(new Long(1), new Long(1), new Long(1));
        ticket2 = new Ticket(new Long(2), new Long(2), new Long(1));
        ticket3 = new Ticket(new Long(3), new Long(3), new Long(1));
        ticket4 = new Ticket(new Long(4), new Long(4), new Long(2));

        dataShow.showAllData();
        if (response != null) response.getWriter().println("inited");
    }
}
