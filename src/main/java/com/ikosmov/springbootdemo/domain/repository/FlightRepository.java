package com.ikosmov.springbootdemo.domain.repository;

import com.ikosmov.springbootdemo.domain.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight,Long> {
    void deleteById(Long id);
    List<Flight> findAll();
    Optional<Flight> findById(Long id);
}
