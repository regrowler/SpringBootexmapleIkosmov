package com.ikosmov.springbootdemo.controller;


import com.ikosmov.springbootdemo.domain.dao.PassengerDao;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class PassengerController {
    @Autowired
    PassengerDao dao;
    @RequestMapping("/passengers")
    public void getAll(HttpServletResponse response) throws IOException {
        
        PrintWriter writer=response.getWriter();
        dao.getPassengers().forEach(writer::println);
    }
    @RequestMapping(value = "/passengers",method = RequestMethod.POST)
    public void add(HttpServletResponse response, @RequestHeader String name) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.postPassenger(name));
    }
    @RequestMapping("/passengers/{id}")
    public void getById(HttpServletResponse response, @PathVariable long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.getPassenger(id));
    }
    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.POST)
    public void putPassenger(HttpServletResponse response,
                             @PathVariable Long id,
                             @RequestHeader String name) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.postPassenger(id,name));
    }
    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.DELETE)
    public void deletePassenger(HttpServletResponse response,
                                @PathVariable Long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.deletePassenger(id));
    }
    @RequestMapping(value = "/passengers/{id}/flights")
    public void passengersFlights(HttpServletResponse response,
                                  @PathVariable Long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        Passenger passenger=dao.getPassenger(id);
        if(passenger!=null){
            writer.println(new Response("ok",dao.getPassengersFlights(id)));
        }else writer.println(new Response("not found",null));
    }
    @RequestMapping(value = "/passengers/{id}/tickets")
    public void passengersTickets(HttpServletResponse response,
                                  @PathVariable Long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        Passenger passenger=dao.getPassenger(id);
        if(passenger!=null){
            writer.println(new Response("ok",dao.getPassengersTickets(id)));
        }else writer.println(new Response("not found",null));
    }
}
