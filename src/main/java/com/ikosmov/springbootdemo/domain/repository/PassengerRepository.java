package com.ikosmov.springbootdemo.domain.repository;

import com.ikosmov.springbootdemo.domain.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger,Long> {
    void deleteById(Long id);
    List<Passenger> findAll();
    Optional<Passenger> findById(Long id);
}
