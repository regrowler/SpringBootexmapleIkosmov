package com.ikosmov.springbootdemo.domain.dao;


import com.ikosmov.springbootdemo.domain.model.Flight;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.domain.repository.FlightRepository;
import com.ikosmov.springbootdemo.domain.repository.PassengerRepository;
import com.ikosmov.springbootdemo.domain.repository.TicketRepository;
import com.ikosmov.springbootdemo.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class TicketDao {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    FlightRepository flightRepository;
    @Autowired
    FlightDao flightDao;
    @Autowired
    PassengerDao passengerDao;

    public List<Ticket> getTickets() {
        return ticketRepository.findAll();
    }

    public Ticket getTicket(Long id) {
        Optional<Ticket> ticket=ticketRepository.findById(new Long(id));
        if(ticket.isPresent())return ticket.get();
        return null;
    }

    public Response postTicket(Long id, Long passenger, Long flight) {

        Optional<Ticket> ticket1 = ticketRepository.findById(new Long(id));
        Ticket ticket = ticket1.orElse(null);
        Optional<Flight> flight1 = flightRepository.findById(new Long(flight));
        Optional<Passenger> passenger1 = passengerRepository.findById(new Long(passenger));
        if (!ticket1.isPresent()) {
            ticket = new Ticket(id, passenger, flight);
            if (!flight1.isPresent()) {
                return new Response("flight not found", null);

            }
            if (!passenger1.isPresent()) {
                return new Response("flight not found", null);
            }
            ticketRepository.save(ticket);

        } else {
            if (!flight1.isPresent()) {
                return new Response("flight not found", null);

            }
            if (!passenger1.isPresent()) {
                return new Response("flight not found", null);
            }

        }

        ticket.setFlight(flight1.get());
        ticket.setPassenger(passenger1.get());
        ticketRepository.save(ticket);
        flightDao.addPassenger(flight,passenger);
        flightDao.addTicket(flight,id);
        passengerDao.addFlight(passenger,flight);
        passengerDao.addTicket(passenger,id);
        return new Response("ok", ticket);
    }

    public Response deleteTicket(Long id) {
        Ticket ticket = ticketRepository.findById(new Long(id)).get();
        ticketRepository.deleteById(new Long(id));
        return new Response("ok", ticket);
    }
}
