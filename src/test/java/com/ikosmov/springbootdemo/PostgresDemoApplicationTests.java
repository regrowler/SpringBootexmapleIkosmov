package com.ikosmov.springbootdemo;

import com.ikosmov.springbootdemo.controller.MainController;
import com.ikosmov.springbootdemo.domain.dao.FlightDao;
import com.ikosmov.springbootdemo.domain.dao.PassengerDao;
import com.ikosmov.springbootdemo.domain.dao.TicketDao;
import com.ikosmov.springbootdemo.domain.model.Flight;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.utils.Response;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class PostgresDemoApplicationTests {
    @Autowired
    WebApplicationContext context;
    @Autowired
    PassengerDao passengerDao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    TicketDao ticketDao;
    Ticket ticket1 = new Ticket(new Long(1), new Long(1), new Long(1));
    Ticket ticket2 = new Ticket(new Long(2), new Long(2), new Long(1));
    Ticket ticket3 = new Ticket(new Long(3), new Long(3), new Long(1));
    Ticket ticket4 = new Ticket(new Long(4), new Long(4), new Long(2));
    Passenger passenger1 = new Passenger(new Long(1), "John");
    Passenger passenger2 = new Passenger(new Long(2), "Andrew");
    Passenger passenger3 = new Passenger(new Long(3), "Michael");
    Passenger passenger5 = new Passenger(new Long(4), "DIma");
    Flight flight1 = new Flight(new Long(1), "Samara", "Moscow");
    Flight flight2 = new Flight(new Long(2), "Samara", "Saratov");

    @Test
    public void testTicketDao() throws Exception {
        MainController controller = context.getBean(MainController.class);
        controller.init(null);

        List<Ticket>list=Stream.of(ticket1,ticket2,ticket3,ticket4).collect(Collectors.toList());
        System.out.println("Get /tickets/");
        ticketDao.getTickets().forEach(System.out::println);
        Assert.assertEquals(list,ticketDao.getTickets());
        System.out.println("Get /tickets/1");
        Assert.assertEquals(ticket1,ticketDao.getTicket(new Long(1)));
        Ticket ticket=new Ticket(5,1,2);
        System.out.println("Post /tickets/ /tickets/5");
        Assert.assertEquals(new Response("ok",ticket),ticketDao.postTicket(new Long(5),new Long(1),new Long(2)));
        System.out.println("Delete /tickets/1");
        Assert.assertEquals(new Response("ok",ticket1),ticketDao.deleteTicket(new Long(1)));
        System.out.println("Get /tickets/2/passenger");
        Assert.assertEquals(passenger2,ticketDao.getTicket(new Long(2)).getPassenger());
        System.out.println("Get /tickets/2/flight");
        Assert.assertEquals(flight1,ticketDao.getTicket(new Long(2)).getFlight());

        List<Flight>list1=Stream.of(flight1,flight2).collect(Collectors.toList());
        System.out.println("Get /flights/");
        flightDao.getFlights().forEach(System.out::println);
        Assert.assertEquals(list1,flightDao.getFlights());
        System.out.println("Get /flights/1");
        Assert.assertEquals(flight1,flightDao.getFlight(new Long(1)));
        Flight flight=new Flight(3,"samara","london");
        System.out.println("Post /flights/ /flights/3");
        Assert.assertEquals(new Response("ok",flight),flightDao.postFlight(new Long(3),"samara","london"));
        System.out.println("Delete /flights/3");
        Assert.assertEquals(new Response("ok",flight),flightDao.deleteFlight(new Long(3)));
        System.out.println("Get /flights/1/tickets");
        Assert.assertEquals(Stream.of(ticket2,ticket3).collect(Collectors.toList()),
                flightDao.getFlightTickets(new Long(1)));
        System.out.println("Get /flights/1/passengers");
        Assert.assertEquals(Stream.of(passenger1,passenger2,passenger3).collect(Collectors.toSet()),
                flightDao.getFlightPassengers(new Long(1)));

        List<Passenger>list2=Stream.of(passenger1,passenger2,passenger3,passenger5).collect(Collectors.toList());
        System.out.println("Get /passengers/");
        flightDao.getFlights().forEach(System.out::println);
        Assert.assertEquals(list2,passengerDao.getPassengers());
        System.out.println("Get /passengers/1");
        Assert.assertEquals(passenger1,passengerDao.getPassenger(new Long(1)));
        Passenger passenger=new Passenger(5,"guy");
        System.out.println("Post /passengers/ /passengers/5");
        Assert.assertEquals(new Response("ok",passenger),passengerDao.postPassenger(new Long(5),"guy"));
        System.out.println("Delete /passengers/3");
        Assert.assertEquals(new Response("ok",passenger),passengerDao.deletePassenger(new Long(5)));
        System.out.println("Get /passengers/2/tickets");
        Assert.assertEquals(Stream.of(ticket2).collect(Collectors.toList()),
                passengerDao.getPassengersTickets(new Long(2)));
        System.out.println("Get /passengers/1/flights");
        Assert.assertEquals(Stream.of(flight1,flight2).collect(Collectors.toSet()),
                passengerDao.getPassengersFlights(new Long(1)));
    }

    @Test
    public void testFlightDao() {

    }

    @Test
    public void testPassengersDao() {

    }

}
