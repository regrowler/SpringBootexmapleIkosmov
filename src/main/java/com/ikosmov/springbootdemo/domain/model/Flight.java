package com.ikosmov.springbootdemo.domain.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;
import java.util.*;

@Entity
public class Flight{
    public Flight(Long id, String startPoLong, String targetPoLong) {
        this.id = id;
        this.startPoLong = startPoLong;
        this.targetPoLong = targetPoLong;
    }
    public Flight(int id, String startPoLong, String targetPoLong) {
        this.id = new Long(id);
        this.startPoLong = startPoLong;
        this.targetPoLong = targetPoLong;
    }
    @Id
    private Long id;
    public String startPoLong;
    public String targetPoLong;

    @JsonIgnore
    @OneToMany(mappedBy = "flight",fetch = FetchType.EAGER)
    private List<Ticket> tickets;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Passenger> passengers;

    public Flight() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long flight_id) {
        this.id = flight_id;
    }

    public String getstartPoLong() {
        return startPoLong;
    }

    public void setstartPoLong(String startPoLong) {
        this.startPoLong = startPoLong;
    }

    public String gettargetPoLong() {
        return targetPoLong;
    }

    public void settargetPoLong(String targetPoLong) {
        this.targetPoLong = targetPoLong;
    }

    public List<Ticket> getTickets() {
        //System.out.println(tickets.size());
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
//        System.out.println(tickets.size());
    }

    public void setPassengers(Set<Passenger> passengers) {
        this.passengers = passengers;
    }

    public Set<Passenger> getPassengers() {
        return passengers;
    }
    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder builder=new StringBuilder(
                "Flight{" +
                        "id=" + id +
                        ", startPoLong='" + startPoLong + '\'' +
                        ", targetPoLong='" + targetPoLong + '\'' +
                        ", tickets='"+tickets+
                        ", passengers='"+passengers+'\''
        );
        if(passengers!=null){
            builder.append(", passengers='"+passengers+"\'\n");
        }
        if(tickets!=null){
            builder.append(", tickets='"+tickets+"\'\n");
        }
        builder.append("}");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return id.equals(flight.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
