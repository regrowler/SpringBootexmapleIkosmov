package com.ikosmov.springbootdemo.controller;


import com.ikosmov.springbootdemo.domain.dao.FlightDao;
import com.ikosmov.springbootdemo.domain.dao.PassengerDao;
import com.ikosmov.springbootdemo.domain.dao.TicketDao;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class TicketController {
    @Autowired
    TicketDao dao;
    @Autowired
    FlightDao flightDao;
    @Autowired
    PassengerDao passengerDao;

    @RequestMapping("/tickets")
    public void getAll(HttpServletResponse response) throws IOException {
        
        PrintWriter writer=response.getWriter();
        dao.getTickets().forEach(writer::println);
    }
    @RequestMapping(value = "/tickets",method = RequestMethod.POST)
    public void add(HttpServletResponse response,
                    @RequestHeader Long id,
                    @RequestHeader Long flight,
                    @RequestHeader Long passenger) throws IOException {
        PrintWriter writer=response.getWriter();
        writer.println(dao.postTicket(id,passenger,flight));
    }
    @RequestMapping("/tickets/{id}")
    public void getById(HttpServletResponse response, @PathVariable Long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.getTicket(id));
    }
    @RequestMapping(value = "/tickets/{id}",method = RequestMethod.POST)
    public void putTicket(HttpServletResponse response,
                          @PathVariable Long id,
                          @RequestHeader Long flight,
                          @RequestHeader Long passenger) throws IOException {
        PrintWriter writer=response.getWriter();
        writer.println(dao.postTicket(id,passenger,flight));
    }
    @RequestMapping(value = "/tickets/{id}",method = RequestMethod.DELETE)
    public void deleteTicket(HttpServletResponse response,
                             @PathVariable Long id) throws IOException {
        
        PrintWriter writer=response.getWriter();
        writer.println(dao.deleteTicket(id));
    }
    @RequestMapping(value = "/tickets/{id}/flight")
    public void getFlight(HttpServletResponse response,
                          @PathVariable Long id) throws IOException {
        Ticket ticket=dao.getTicket(id);
        PrintWriter writer=response.getWriter();
        if(ticket==null){
            writer.println(new Response("ticket not found",null));
            return;
        }
        writer.println(new Response("ok",ticket.getFlight()));
    }
    @RequestMapping(value = "/tickets/{id}/passenger")
    public void getPassenger(HttpServletResponse response,
                             @PathVariable Long id) throws IOException {
        Ticket ticket=dao.getTicket(id);
        PrintWriter writer=response.getWriter();
        if(ticket==null){
            writer.println(new Response("ticket not found",null));
            return;
        }
        writer.println(new Response("ok",ticket.getPassenger()));
    }
//    @RequestMapping(value = "/tickets/{id}/passenger",method = RequestMethod.POST)
//    public void setPassenger(HttpServletResponse response,
//                             @PathVariable Long id,
//                             @RequestHeader Long passenger) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.setPassengerId(ticket,passenger));
//    }
//    @RequestMapping(value = "/tickets/{id}/flight",method = RequestMethod.POST)
//    public void setFlight(HttpServletResponse response,
//                          @PathVariable Long id,
//                          @RequestHeader Long flight) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.setFlightId(ticket,flight));
//    }
//    @RequestMapping(value = "/tickets/{id}/passenger",method = RequestMethod.DELETE)
//    public void resetPassenger(HttpServletResponse response,
//                             @PathVariable Long id) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("passenger not found",null));
//            return;
//        }
//        writer.println(dao.resetPassenger(ticket));
//    }
//    @RequestMapping(value = "/tickets/{id}/flight",method = RequestMethod.DELETE)
//    public void resetFlight(HttpServletResponse response,
//                          @PathVariable Long id) throws IOException {
//        Ticket ticket=dao.getTicket(id);
//        PrintWriter writer=response.getWriter();
//        if(ticket==null){
//            writer.println(new Response("ticket not found",null));
//            return;
//        }
//        writer.println(dao.resetFlight(ticket));
//    }
}
