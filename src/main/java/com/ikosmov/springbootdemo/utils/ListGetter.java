package com.ikosmov.springbootdemo.utils;

import java.util.*;

public class ListGetter {
    public static <T> List<T> getListFromEntity(T entity) {
        ArrayList<T> arrayList = new ArrayList<>();
        arrayList.add(entity);
        return arrayList;
    }
    public static <T> Set<T> getSetFromEntity(T ... entity){
        HashSet<T> set=new HashSet<>();
        Arrays.stream(entity).forEach(set::add);
        return set;
    }
}
