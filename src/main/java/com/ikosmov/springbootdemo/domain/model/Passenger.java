package com.ikosmov.springbootdemo.domain.model;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;
import java.util.*;

@Entity
public class Passenger{
    public Passenger(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    public Passenger(int id, String name) {
        this.id = new Long(id);
        this.name = name;
    }
    @Id
    private Long id;

    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "passenger",fetch = FetchType.EAGER)
    private List<Ticket> tickets;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "passengers_flihts",
            joinColumns = @JoinColumn(name = "fligt_id"),
            inverseJoinColumns = @JoinColumn(name = "passenger_id")
    )
    private Set<Flight> flights;

    public Passenger() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long passenger_id) {
        this.id = passenger_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }



    public Set<Flight> getFlights() {
        return flights;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void setFlights(Set<Flight> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        ObjectMapper mapper=new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder builder=new StringBuilder(
                "Passenger{" +
                "id=" + id +
                ", name='" + name + '\'');
        if(tickets!=null){
            builder.append("\n, tickets=\'"+tickets+"\'");
        }
        if(flights!=null){
            builder.append("\n, flights=\'"+flights+"\'");
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return id.equals(passenger.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Passenger(Passenger passenger) {
        this.id = passenger.id;
        this.name = passenger.name;
        this.tickets=new ArrayList<>();
        this.flights = new HashSet<>();
    }
}
