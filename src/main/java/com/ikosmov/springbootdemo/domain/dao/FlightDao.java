package com.ikosmov.springbootdemo.domain.dao;


import com.ikosmov.springbootdemo.domain.model.Flight;
import com.ikosmov.springbootdemo.domain.model.Passenger;
import com.ikosmov.springbootdemo.domain.model.Ticket;
import com.ikosmov.springbootdemo.domain.repository.FlightRepository;
import com.ikosmov.springbootdemo.utils.Response;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class FlightDao {

    @Autowired
    FlightRepository flightRepository;
    @Autowired
    TicketDao ticketDao;
    @Autowired
    PassengerDao passengerDao;

    public List<Flight> getFlights() {
        return flightRepository.findAll();
    }

    public Flight getFlight(Long id) {
        Optional<Flight> flight = flightRepository.findById(new Long(id));
        if (flight.isPresent()) return flight.get();
        return null;
    }

    public List<Ticket> getFlightTickets(Long id) {
        Flight flight = getFlight(id);
        if (flight == null) return null;
        List list = flight.getTickets();
        HashSet set=new HashSet();
        set.addAll(list);
        return (List<Ticket>) set.stream().collect(Collectors.toList());
    }

    public Set<Passenger> getFlightPassengers(Long id) {
        Flight flight = getFlight(id);
        if (flight == null) return null;
        Set set = flight.getPassengers();
        return set;
    }

    public Response postFlight(Long id, String startPoLong, String targetPoLong) {
        Flight flight = new Flight(id, startPoLong, targetPoLong);
        flightRepository.save(flight);
        return new Response("ok", flight);
    }

    public Response deleteFlight(Long id) {
        Optional<Flight> flight = flightRepository.findById(id);
        if (flight.isPresent()) {
            flightRepository.deleteById(id);
            return new Response("ok", flight.get());
        }
        return new Response("not found", null);
    }

    public Response addPassenger(Long flightId, Long id) {
        Flight flight = getFlight(flightId);
        if (flight == null) new Response("flight not found", null);
        Passenger passenger = passengerDao.getPassenger(id);
        if (passenger == null) new Response("passenger not found", null);
        if (flight.getPassengers().contains(passenger)) flight.getPassengers().add(passenger);
        flightRepository.save(flight);
        return new Response("ok", flight);
    }

    public Response removeFlight(Flight flight, Long id) {
        if (flight == null) new Response("flight not found", null);
        Passenger passenger = passengerDao.getPassenger(id);
        if (passenger == null) new Response("passenger not found", null);
        if (flight.getPassengers().contains(passenger)) {
            flight.getPassengers().remove(passenger);
        }
        flightRepository.save(flight);
        return new Response("ok", flight);
    }

    public Response removeTicket(Flight flight, Long id) {
        Ticket ticket = ticketDao.getTicket(id);
        if (ticket != null) {
            List set = flight.getTickets();
            if (set.contains(ticket)) {
                set.remove(ticket);
                set.add(ticket);
            } else set.add(ticket);
            flight.setTickets(set);
            return new Response("ok", ticket);
        }
        return new Response("flight not found", null);
    }

    public Response addTicket(Long flightId, Long id) {
        Ticket ticket = ticketDao.getTicket(id);
        Optional<Flight> flight = flightRepository.findById(new Long(flightId));
        if (ticket != null && flight.isPresent()) {
            Flight flight1 = flight.get();
            if (!flight1.getTickets().contains(ticket)) {
                flight1.getTickets().add(ticket);
            }
            flightRepository.save(flight1);
            return new Response("ok", ticket);
        }
        return new Response("smth not found", null);
    }
}
