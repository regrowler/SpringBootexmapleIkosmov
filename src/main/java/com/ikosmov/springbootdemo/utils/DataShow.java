package com.ikosmov.springbootdemo.utils;

import com.ikosmov.springbootdemo.domain.dao.FlightDao;
import com.ikosmov.springbootdemo.domain.dao.PassengerDao;
import com.ikosmov.springbootdemo.domain.dao.TicketDao;
import com.ikosmov.springbootdemo.domain.model.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class DataShow {
    @Value("${show_data}")
    private boolean showData;
    @Autowired
    TicketDao ticketDao;
    @Autowired
    PassengerDao passengerDao;
    @Autowired
    FlightDao flightDao;

    public void showAllData() {
        if (showData) {
            System.out.println("=======================================================================================");
            ticketDao.getTickets().forEach(System.out::println);
            passengerDao.getPassengers().forEach(System.out::println);
            flightDao.getFlights().forEach(System.out::println);
            showFlightsTickets();
            showFlightsPassengers();

        }
    }

    public void showFlightsTickets() {
        if (showData) {
            List<Flight> list = flightDao.getFlights();
            if (list.size() > 0) {
                System.err.println("\ntickets");
            list.forEach(flight -> {

                    System.out.println("=====================flight" + flight.getId() + "===========================================================");
                    flightDao.getFlightTickets(flight.getId()).forEach(System.out::println);
                });
            }
        }
    }
    public void showFlightsPassengers() {
        if (showData) {
            List<Flight> list = flightDao.getFlights();
            if (list.size() > 0) {
                System.err.println("\npassengers");
                list.forEach(flight -> {

                    System.out.println("=====================flight" + flight.getId() + "===========================================================");
                    flightDao.getFlightPassengers(flight.getId()).forEach(System.out::println);
                });
            }
        }
    }
}
